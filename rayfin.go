// Package rayfin implements the network communication protocol with a
// Subsea Rayfin camera.
package rayfin

import (
	"encoding/binary"
	"fmt"
	"io"
	"strings"
)

// Reader reads length-prefixed strings from an underlying io.Reader
type Reader struct {
	rdr io.Reader
}

func NewReader(r io.Reader) *Reader {
	return &Reader{rdr: r}
}

// ReadString returns the next string sent by a Rayfin camera along with any
// error that occurs.
func (r *Reader) ReadString() (string, error) {
	var size uint32
	err := binary.Read(r.rdr, binary.LittleEndian, &size)
	if err != nil {
		return "", fmt.Errorf("Cannot read string length: %w", err)
	}
	p := make([]byte, int(size))
	_, err = io.ReadFull(r.rdr, p)

	return string(p), err
}

// Writer writes length-prefixed strings to an underlying io.Writer
type Writer struct {
	wtr io.Writer
}

func NewWriter(w io.Writer) *Writer {
	return &Writer{wtr: w}
}

// Write string sends a string to a Rayfin camera
func (w *Writer) WriteString(s string) error {
	size := uint32(len(s))
	err := binary.Write(w.wtr, binary.LittleEndian, size)
	if err != nil {
		return fmt.Errorf("Cannot write string length: %w", err)
	}
	_, err = w.wtr.Write([]byte(s))
	return err
}

const linefeedSeq = "`%"

// EscapeScript converts all linefeeds in a script to the "`%" character
// sequence.
func EscapeScript(in string) string {
	return strings.ReplaceAll(in, "\n", linefeedSeq)
}

// UnescapeScript converts all "`%" sequences in a script to linefeeds
func UnescapeScript(in string) string {
	return strings.ReplaceAll(in, linefeedSeq, "\n")
}
