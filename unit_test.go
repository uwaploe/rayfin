package rayfin

import (
	"bytes"
	"testing"
)

func TestReader(t *testing.T) {
	table := []struct {
		in  []byte
		out []string
	}{
		{
			in:  []byte("\x0e\x00\x00\x00StartRecording"),
			out: []string{"StartRecording"},
		},
		{
			in:  []byte("\x0b\x00\x00\x00hello world\x03\x00\x00\x00bye"),
			out: []string{"hello world", "bye"},
		},
	}

	var b bytes.Buffer
	rdr := NewReader(&b)
	for _, e := range table {
		b.Write(e.in)
		for _, text := range e.out {
			s, err := rdr.ReadString()
			if err != nil {
				t.Fatal(err)
			}
			if s != text {
				t.Errorf("Bad value; expected %q, got %q",
					text, s)
			}
		}
	}
}

func TestWriter(t *testing.T) {
	table := []struct {
		out string
		in  []string
	}{
		{
			out: "\x0e\x00\x00\x00StartRecording",
			in:  []string{"StartRecording"},
		},
		{
			out: "\x0b\x00\x00\x00hello world\x03\x00\x00\x00bye",
			in:  []string{"hello world", "bye"},
		},
	}

	var b bytes.Buffer
	wtr := NewWriter(&b)

	for _, e := range table {
		for _, text := range e.in {
			wtr.WriteString(text)
		}
		s := b.String()
		if s != e.out {
			t.Errorf("Bad value; expected %q, got %q",
				e.out, s)
		}
		b.Reset()
	}
}
